package com.heaven.mapper;

import com.heaven.pojo.Nav;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface NavMapper {

    @Select("select * from nav")
    List<Nav> getNavList();
}
