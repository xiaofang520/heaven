package com.heaven.service;

import com.heaven.pojo.User;

public interface UserService {
    String login(User user);
}
