package com.heaven.service;

import com.heaven.mapper.UserMapper;
import com.heaven.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public String login(User user) {
        String password = user.getPassword();
        String md = DigestUtils.md5DigestAsHex(password.getBytes());
        user.setPassword(md);
        User LoginUser = userMapper.login(user);
        if(LoginUser == null){
            return null;
        }
        String token = UUID.randomUUID().toString();
        return token;
    }
}
