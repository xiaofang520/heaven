package com.heaven.service;

import com.heaven.pojo.Nav;

import java.util.List;

public interface NavService {
    List<Nav> getNavList();
}
