package com.heaven.service;

import com.heaven.mapper.NavMapper;
import com.heaven.pojo.Nav;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NavServiceImpl implements NavService {

    @Autowired
    private NavMapper navMapper;

    @Override
    public List<Nav> getNavList() {

        return navMapper.getNavList();
    }
}
