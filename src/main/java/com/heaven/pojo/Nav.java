package com.heaven.pojo;

import lombok.Data;

import java.io.Serializable;

@Data
public class Nav implements Serializable {
    private Integer id;
    private String name;
    private String parentId;
    private Boolean level;
    private String path;
}
