package com.heaven.controller;

import com.heaven.pojo.User;
import com.heaven.service.UserService;
import com.heaven.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/user")
public class Usercontroller {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public SysResult login(@RequestBody User user){
        //执行后端登录操作,要求返回token密钥
        String token = userService.login(user);
        //判断token返回值是否有效
        if(StringUtils.hasLength(token)){
            return SysResult.success(token);
        }else {
            //登陆失败
            return SysResult.fail();
        }
    }
}
