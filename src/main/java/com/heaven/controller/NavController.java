package com.heaven.controller;

import com.heaven.pojo.Nav;
import com.heaven.service.NavService;
import com.heaven.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/nav")
public class NavController {

    @Autowired
    private NavService navService;

    /**
     * 请求路径 /nav/getNavList
     * 请求类型 GET
     * 请求参数 无
     * 响应数据 SysResult对象
     */

    @GetMapping("/getNavList")
    public SysResult getNavList(){
        List<Nav> list = navService.getNavList();
        return SysResult.success(list);
    }
}
