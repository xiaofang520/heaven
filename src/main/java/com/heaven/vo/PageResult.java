package com.heaven.vo;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PageResult {
    private String query;//用户查询的条件 根据用户名查询
    private Integer pageNum;//分页的页数
    private Integer pageSize;//分页的条数
    private Long total; //数据总数
    private Object rows; //分页的结果
}

